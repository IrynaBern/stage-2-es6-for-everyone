class Fighter{
	
	constructor(name,  health, attack, defense){
		this.name = name;
		this.health = health;
        this.attack = attack;
        this.defense = defense;
	}

	getName(){
		return this.name;
	}
	getHealth(){
		return this.health;
	}

	getAttack(){
		return this.attack;
	}

	getDefense(){
		return this.defense;
	}

	setHealth(health){
		this.health = health;
	}

	setAttack(attack){
		this.attack = attack;
	}

	setDefense(defense){
		this.defense = defense;
	}	

	getHitPower(){
		let power;
		const min = 1;
		const max = 2;
		let criticalHitChance = Math.floor(Math.random() * (max - min + 1) ) + min;
		power = attack * criticalHitChance;
		return power;
	}

	getBlockPower(){
		let power;
		const min = 1;
		const max = 2;
		let dodgeChance = Math.floor(Math.random() * (max - min + 1) ) + min;		
		power = defense * dodgeChance;
		return power;
	}
	
	hit(enemy){
		enemy.setHealth(getHitPower - enemy.getBlockPower());
	}

	knockout(){
		console.log('time is over');
		return new Promise((resolve, reject) => {
		setTimeout(resolve, 500);
		});
	}
};

async function fight(fighter1, fighter2){
	const min = 1;
	const max = 2;
	while (fighter1.health > 0 && fighter2.health > 0)
	{
		let order = Math.floor(Math.random() * (max - min + 1) ) + min;
		switch (order){
		    case 1: fighter1.hit(fighter2); break;
			case 2: fighter2.hit(fighter1); break;
		}
		if(fighter1.health <= 0)
		{
			try{
				const  f = await fighter1.knockout();
				console.log(`${fighter1} in knockout`);
			}catch(error){
				console.log(error);
			}
		}
		else
		{
			try{
				const f = await fighter2.knockout();
				console.log(`${fighter2} in knockout`);
			}catch(error){
				console.log(error);
			}
		}
	}
}

export default Fighter;